const replace = require('replace-in-file');
const fs = require('fs-extra')
const path = require('path')

const myArgs = process.argv.slice(2);
const subPath = myArgs[0] || ''
const shouldWrite = myArgs[1] === 'write'
const scanDirectory = path.join(__dirname, subPath)
console.log(scanDirectory)
/**
 * Find/Replace escaped characters in all XML files in a directory
 */

const characterMap = [
  [ '&Eacute;',	'É' ],
  [ '&Aacute;',	'Á' ],
  [ '&Iacute;',	'Í' ],
  [ '&Oacute;',	'Ó' ],
  [ '&Ntilde;',	'Ñ' ],
  [ '&Uacute;',	'Ú' ],
  [ '&Uuml;',	'Ü' ],
  [ '&iexcl;',	'¡' ],
  [ '&iquest;',	'¿' ],
  [ '&aacute;',	'á' ],
  [ '&eacute;',	'é' ],
  [ '&iacute;',	'í' ],
  [ '&oacute;',	'ó' ],
  [ '&ntilde;',	'ñ' ],
  [ '&uacute;',	'ú' ],
  [ '&uuml;',	'ü' ],
  [ '&ordf;',	'ª' ],
  [ '&ordm;',	'º' ]
]

const fromMap = characterMap.map((tuple) => {
  return new RegExp(tuple[0], 'g')
})

const toMap = characterMap.map((tuple) => tuple[1])

console.log('from', fromMap)
console.log('to', toMap)
const options = {
  files: [scanDirectory + '/*.xml'],
    from: fromMap,
    to: toMap,
    countMatches: true,
}

const results = replace.sync(options)

console.log(results)
