export = [
  { &Eacute;,	É },
  { &Aacute;,	Á },
  { &Iacute;,	Í },
  { &Oacute;,	Ó },
  { &Ntilde;,	Ñ },
  { &Uacute;,	Ú },
  { &Uuml;,	Ü },
  { &iexcl;,	¡ },
  { &iquest;,	¿ },
  { &aacute;,	á },
  { &eacute;,	é },
  { &iacute;,	í },
  { &oacute;,	ó },
  { &ntilde;,	ñ },
  { &uacute;,	ú },
  { &uuml;,	ü },
  { &ordf;,	ª },
  { &ordm;,	º }
]
